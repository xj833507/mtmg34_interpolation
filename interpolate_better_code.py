import cartopy.feature as cfeature
from matplotlib.colors import BoundaryNorm
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mtick

from metpy.cbook import get_test_data
from metpy.interpolate import (interpolate_to_grid, remove_nan_observations,
                               remove_repeat_coordinates)

A_vai_temp_0950 = (-1.994312, 50.604919, 8.10)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.73)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.3)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.13)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.3)
G_vai_temp_0950 = (-1.995829, 50.607465, 8.3)

combined_0950 = np.zeros([6,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = D_vai_temp_0950
combined_0950[3] = E_vai_temp_0950
combined_0950[4] = F_vai_temp_0950
combined_0950[5] = G_vai_temp_0950

plot_0950 = (np.zeros(6), np.zeros(6), np.zeros(6))

lons = np.array([])
lats = np.array([])
temps = np.array([])
text = np.array(['A','B','D','E','F','G'])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)

levels = np.arange(6, 12.1, 0.1)
cmap = plt.get_cmap('jet')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

x, y, temp = lons, lats, temps

gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.00004)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels, ticks=[6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0], format='%.1f', label=r'Temperature ($^\circ C$)')
ax = plt.gca()
font = {'size' : 14}
plt.rc('font', **font)
for i in np.arange(6):
    plt.plot(lons[i],lats[i],'.k')
    ax.annotate(text[i],(lons[i]+0.00002,lats[i]+0.00002))
plt.plot(-1.99459, 50.60712,'.k')
ax.annotate('C',(-1.99459+0.00002,50.60712+0.00002))

ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.ylim([np.min(lats)-0.0002,np.max(lats)+0.0002])
plt.xlim([np.min(lons)-0.0002,np.max(lons)+0.0002])
plt.xlabel(r'Longitude ($^\circ$)')
plt.ylabel(r'Latitude ($^\circ$)')
plt.tight_layout()
plt.savefig("0950_0.pdf")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.10)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.73)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.3)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.13)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.3)
G_vai_temp_0950 = (-1.995829, 50.607465, 8.3)

combined_0950 = np.zeros([6,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = D_vai_temp_0950
combined_0950[3] = E_vai_temp_0950
combined_0950[4] = F_vai_temp_0950
combined_0950[5] = G_vai_temp_0950

plot_0950 = (np.zeros(6), np.zeros(6), np.zeros(6))

lons = np.array([])
lats = np.array([])
temps = np.array([])
text = np.array(['A','B','D','E','F','G'])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)

levels = np.arange(6, 12.1, 0.1)
cmap = plt.get_cmap('jet')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

x, y, temp = lons, lats, temps

gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.00004)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels, ticks=[6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0], format='%.1f', label=r'Temperature ($^\circ C$)')
ax = plt.gca()
font = {'size' : 14}
plt.rc('font', **font)
for i in np.arange(6):
    plt.plot(lons[i],lats[i],'.k')
    ax.annotate(text[i],(lons[i]+0.00002,lats[i]+0.00002))
plt.plot(-1.99459, 50.60712,'.k')
ax.annotate('C',(-1.99459+0.00002,50.60712+0.00002))

ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=9)
ax.tick_params(axis="y", labelsize=9)
plt.ylim([np.min(lats)-0.0002,np.max(lats)+0.0002])
plt.xlim([np.min(lons)-0.0002,np.max(lons)+0.0002])
plt.xlabel(r'Longitude ($^\circ$)')
plt.ylabel(r'Latitude ($^\circ$)')
plt.tight_layout()
plt.savefig("0950.pdf")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.3)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.43)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.5)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.63)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.2)
G_vai_temp_0950 = (-1.995829, 50.607465, 11.2)

combined_0950 = np.zeros([6,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = D_vai_temp_0950
combined_0950[3] = E_vai_temp_0950
combined_0950[4] = F_vai_temp_0950
combined_0950[5] = G_vai_temp_0950

plot_0950 = (np.zeros(6), np.zeros(6), np.zeros(6))

lons = np.array([])
lats = np.array([])
temps = np.array([])
text = np.array(['A','B','D','E','F','G'])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)

levels = np.arange(6, 12.1, 0.1)
cmap = plt.get_cmap('jet')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

x, y, temp = lons, lats, temps

gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.00004)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels, ticks=[6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0], format='%.1f', label=r'Temperature ($^\circ C$)')
ax = plt.gca()
font = {'size' : 14}
plt.rc('font', **font)
for i in np.arange(6):
    plt.plot(lons[i],lats[i],'.k')
    ax.annotate(text[i],(lons[i]+0.00002,lats[i]+0.00002))
plt.plot(-1.99459, 50.60712,'.k')
ax.annotate('C',(-1.99459+0.00002,50.60712+0.00002))

ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=9)
ax.tick_params(axis="y", labelsize=9)
plt.ylim([np.min(lats)-0.0002,np.max(lats)+0.0002])
plt.xlim([np.min(lons)-0.0002,np.max(lons)+0.0002])
plt.xlabel(r'Longitude ($^\circ$)')
plt.ylabel(r'Latitude ($^\circ$)')
plt.tight_layout()
plt.savefig("1020.pdf")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 9.1)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.23)
D_vai_temp_0950 = (-1.994420, 50.607785, 9.5)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.93)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.7)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.9)

combined_0950 = np.zeros([6,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = D_vai_temp_0950
combined_0950[3] = E_vai_temp_0950
combined_0950[4] = F_vai_temp_0950
combined_0950[5] = G_vai_temp_0950

plot_0950 = (np.zeros(6), np.zeros(6), np.zeros(6))

lons = np.array([])
lats = np.array([])
temps = np.array([])
text = np.array(['A','B','D','E','F','G'])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)

levels = np.arange(6, 12.1, 0.1)
cmap = plt.get_cmap('jet')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

x, y, temp = lons, lats, temps

gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.00004)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels, ticks=[6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0], format='%.1f', label=r'Temperature ($^\circ C$)')
ax = plt.gca()
font = {'size' : 14}
plt.rc('font', **font)
for i in np.arange(6):
    plt.plot(lons[i],lats[i],'.k')
    ax.annotate(text[i],(lons[i]+0.00002,lats[i]+0.00002))
plt.plot(-1.99459, 50.60712,'.k')
ax.annotate('C',(-1.99459+0.00002,50.60712+0.00002))

ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=9)
ax.tick_params(axis="y", labelsize=9)
plt.ylim([np.min(lats)-0.0002,np.max(lats)+0.0002])
plt.xlim([np.min(lons)-0.0002,np.max(lons)+0.0002])
plt.xlabel(r'Longitude ($^\circ$)')
plt.ylabel(r'Latitude ($^\circ$)')
plt.tight_layout()
plt.savefig("1050.pdf")
plt.clf()

######
