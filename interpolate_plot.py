import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib.colors import BoundaryNorm
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mtick

from metpy.cbook import get_test_data
from metpy.interpolate import (interpolate_to_grid, remove_nan_observations,
                               remove_repeat_coordinates)
from metpy.plots import add_metpy_logo

def basic_map(proj):
    """Make our basic default map for plotting"""
    fig = plt.figure(figsize=(15, 10))
    view = fig.add_axes([0, 0, 1, 1], projection=proj)
    view.set_extent([-1.995, -1.993, 50.609, 50.604])
    view.add_feature(cfeature.STATES.with_scale('50m'))
    view.add_feature(cfeature.OCEAN)
    view.add_feature(cfeature.COASTLINE)
    view.add_feature(cfeature.BORDERS, linestyle=':')
    return fig, view

A_vai_temp_0950 = (-1.994312, 50.604919, 8.10)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.73)
C_vai_temp_0950 = (-1.99459, 50.60712, 11.53)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.3)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.13)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.3)
G_vai_temp_0950 = (-1.995829, 50.607465, 8.3)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("0950.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 7.8)
B_vai_temp_0950 = (-1.994995, 50.60706, 7.83)
C_vai_temp_0950 = (-1.99459, 50.60712, 8.33)
D_vai_temp_0950 = (-1.994420, 50.607785, 6.8)
E_vai_temp_0950 = (-1.993831, 50.608203, 6.73)
F_vai_temp_0950 = (-1.995645, 50.606600, 7.9)
G_vai_temp_0950 = (-1.995829, 50.607465, 8.1)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("0955.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.3)
B_vai_temp_0950 = (-1.994995, 50.60706, 7.83)
C_vai_temp_0950 = (-1.99459, 50.60712, 13.63)
D_vai_temp_0950 = (-1.994420, 50.607785, 6.8)
E_vai_temp_0950 = (-1.993831, 50.608203, 6.63)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.2)
G_vai_temp_0950 = (-1.995829, 50.607465, 9.1)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1000.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.6)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.23)
C_vai_temp_0950 = (-1.99459, 50.60712, 11.53)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.0)
E_vai_temp_0950 = (-1.993831, 50.608203, 6.93)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.7)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.5)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1005.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.2)
B_vai_temp_0950 = (-1.994995, 50.60706, 7.93)
C_vai_temp_0950 = (-1.99459, 50.60712, 9.33)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.1)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.43)
#F_vai_temp_0950 = (-1.995645, 50.606600, nan)
G_vai_temp_0950 = (-1.995829, 50.607465, 9.9)

combined_0950 = np.zeros([6,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
#combined_0950[5] = F_vai_temp_0950
combined_0950[5] = G_vai_temp_0950

plot_0950 = (np.zeros(6), np.zeros(6), np.zeros(6))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1010.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.1)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.23)
C_vai_temp_0950 = (-1.99459, 50.60712, 13.53)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.2)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.03)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.4)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.5)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1015.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.3)
B_vai_temp_0950 = (-1.994995, 50.60706, 8.43)
C_vai_temp_0950 = (-1.99459, 50.60712, 13.53)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.5)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.63)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.2)
G_vai_temp_0950 = (-1.995829, 50.607465, 11.2)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1020.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.6)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.13)
C_vai_temp_0950 = (-1.99459, 50.60712, 11.93)
D_vai_temp_0950 = (-1.994420, 50.607785, 7.6)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.43)
F_vai_temp_0950 = (-1.995645, 50.606600, 8.9)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.5)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1025.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 8.8)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.43)
C_vai_temp_0950 = (-1.99459, 50.60712, 14.23)
D_vai_temp_0950 = (-1.994420, 50.607785, 8.0)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.33)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.2)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.9)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1030.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 9.2)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.13)
C_vai_temp_0950 = (-1.99459, 50.60712, 12.33)
D_vai_temp_0950 = (-1.994420, 50.607785, 8.0)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.63)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.5)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.4)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1035.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 10.3)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.43)
C_vai_temp_0950 = (-1.99459, 50.60712, 13.83)
D_vai_temp_0950 = (-1.994420, 50.607785, 8.2)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.73)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.7)
G_vai_temp_0950 = (-1.995829, 50.607465, 11.6)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1040.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 9.0)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.53)
C_vai_temp_0950 = (-1.99459, 50.60712, 14.03)
D_vai_temp_0950 = (-1.994420, 50.607785, 8.7)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.83)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.2)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.7)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1045.png")
plt.clf()

######

A_vai_temp_0950 = (-1.994312, 50.604919, 9.1)
B_vai_temp_0950 = (-1.994995, 50.60706, 9.23)
C_vai_temp_0950 = (-1.99459, 50.60712, 12.83)
D_vai_temp_0950 = (-1.994420, 50.607785, 9.5)
E_vai_temp_0950 = (-1.993831, 50.608203, 7.93)
F_vai_temp_0950 = (-1.995645, 50.606600, 9.7)
G_vai_temp_0950 = (-1.995829, 50.607465, 10.9)

combined_0950 = np.zeros([7,3])
combined_0950[0] = A_vai_temp_0950
combined_0950[1] = B_vai_temp_0950
combined_0950[2] = C_vai_temp_0950
combined_0950[3] = D_vai_temp_0950
combined_0950[4] = E_vai_temp_0950
combined_0950[5] = F_vai_temp_0950
combined_0950[6] = G_vai_temp_0950

plot_0950 = (np.zeros(7), np.zeros(7), np.zeros(7))

lons = np.array([])
lats = np.array([])
temps = np.array([])

for this_lon in combined_0950[:,0]:
    lons = np.append(lons,this_lon)

for this_lat in combined_0950[:,1]:
    lats = np.append(lats,this_lat)

for this_temp in combined_0950[:,2]:
    temps = np.append(temps,this_temp)


from_proj = ccrs.Geodetic()
to_proj = ccrs.AlbersEqualArea(central_longitude=-1.99, central_latitude=50.60)

levels = list(range(6, 15, 1))
cmap = plt.get_cmap('magma')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

# x, y, temp = station_test_data('air_temperature', from_proj, to_proj)

# x, y, temp = remove_nan_observations(x, y, temp)
# x, y, temp = remove_repeat_coordinates(x, y, temp)

x, y, temp = lons, lats, temps


gx, gy, img = interpolate_to_grid(x, y, temp, interp_type='linear', hres=0.0001)
# plt.pcolormesh(gx,gy,img)
# img = np.ma.masked_where(np.isnan(img), img)
# fig, view = basic_map(to_proj)
mmb = plt.pcolormesh(gx, gy, img, cmap=cmap, norm=norm)
plt.colorbar(mmb, shrink=.4, pad=0, boundaries=levels)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.tick_params(axis="x", labelsize=7)
ax.tick_params(axis="y", labelsize=7)
plt.savefig("1050.png")
plt.clf()
